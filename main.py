from MainClass import MainClass
from ErrorClass import ErrorClass
from json import load, JSONDecodeError
from utils import set_window


def main():
    try:
        settings_file = open('settings.json', 'rt', encoding='utf-8')
    except FileNotFoundError or IsADirectoryError:
        err = ErrorClass('Can`t load settings.json file')
        err.run()
        exit(1)
    try:
        settings = load(settings_file)
    except JSONDecodeError:
        err = ErrorClass('settings.json error')
        err.run()
        exit(2)

    set_window(settings)

    app = MainClass(settings)
    app.run()


if __name__ == '__main__':
    main()
