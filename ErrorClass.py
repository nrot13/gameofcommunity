from direct.showbase.ShowBase import ShowBase
from direct.gui.OnscreenText import OnscreenText


class ErrorClass(ShowBase):
    def __init__(self, msg):
        ShowBase.__init__(self)
        OnscreenText(
            text=msg,
            style=1,
            fg=(1, 1, 1, 1),
        )
