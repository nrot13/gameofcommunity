import config
from os.path import join
from os import listdir
from json import load, JSONDecodeError
from ErrorClass import ErrorClass


class Level:
    def __init__(self, meta, path):
        self.path = path
        self.name = meta["levelname"]
        self.t = self.translate
        self.tr = {}
        locals = listdir(join(path, 'locals'))
        if config.loc in locals:
            loc = config.loc
        else:
            print(locals)
            loc = locals[0]
        file = open(join(path, 'locals', loc), 'rt', encoding='utf-8')
        if not self.init_translate(loc, file):
            err = ErrorClass('Can`t parse local file: {0}'.format(loc))
            err.run()
            exit(3)

    def init_translate(self, loc, file):
        try:
            tr = load(file)
        except JSONDecodeError:
            return False
        self.tr = tr
        return True

    def translate(self, code):
        if code is self.tr:
            return self.tr[code]
        else:
            return code
