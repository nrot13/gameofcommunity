import config
from json import load, JSONDecodeError


def init_translate(loc, file):
    try:
        tr = load(file)
    except JSONDecodeError:
        return False
    config.tr = tr
    config.loc = loc
    return True


def translate(code):
    if code in config.tr:
        return config.tr[code]
    else:
        return code
