from direct.showbase.ShowBase import ShowBase
from os import listdir
from os.path import join
from json import load, JSONDecodeError
from Translate import init_translate
from Translate import translate as t
from ErrorClass import ErrorClass
from Classes.LevelClass import Level
from core.MainMenuRender import MainMenu
from core.LoadGameMenuRender import *
from core.OptionsMenuRender import *

MAINMENU = 0
MAINMENUSETTINGS = 1
MAINCHOICELEVEL = 2
MAINLOAD = 3


# MAINMODS = 4


class MainClass(ShowBase):
    def __init__(self, settings):
        ShowBase.__init__(self)

        self.state = MAINMENU
        self.settings = settings
        self.levels = []

        self.set_setting()
        self.scan_levels()
        self.main_menu()

    def set_setting(self):
        if "lang" in self.settings:
            locals = listdir('locals')
            if self.settings['lang'] in locals:
                loc = self.settings['lang']
            else:
                print(locals)
                loc = locals[0]
            file = open(join('locals', loc), 'rt', encoding='utf-8')
            if not init_translate(loc, file):
                err = ErrorClass('Can`t parse local file: {0}'.format(loc))
                err.run()
                exit(3)
        else:
            err = ErrorClass('Can`t find this language')
            err.run()
            exit(4)

    def scan_levels(self):
        levels_path = 'levels'
        dirs_level = listdir(levels_path)
        for level_dir in dirs_level:
            if "." in level_dir:
                continue
            try:
                meta_file = open(join(levels_path, level_dir, 'meta.json'), 'rt')
            except FileNotFoundError or FileExistsError:
                continue
            try:
                meta = load(meta_file)
            except JSONDecodeError:
                continue
            self.levels.append(Level(meta, join(levels_path, level_dir)))

    def main_menu(self):
        MainMenu(self.load_game, self.options, self.exit)

    def load_game(self):
        pass

    def options(self):
        pass

    def exit(self):
        exit(0)
