from direct.gui.OnscreenText import OnscreenText
from direct.gui.OnscreenImage import OnscreenImage
from direct.gui.DirectGui import *
from panda3d.core import TextNode
from os import listdir
from os.path import join
from random import choice
from Translate import translate as t
from utils import text_to_node as tn


def _head_text():
    head_font = loader.loadFont(join('fonts', 'Neucha.ttf'))
    head_font.setPixelsPerUnit(120)
    head_text, head_size = t('Warhammer 40k: Golden Throne')
    head_size = float(head_size)
    head_obj = OnscreenText(
        text=head_text,
        pos=(0.02, 0.5),
        scale=head_size,
        shadow=(0.05, 0.05, 0.05, 0.1),
        align=TextNode.ACenter,
        font=head_font
    )
    return head_obj, head_font


def _background():
    dir_path = join('resources', 'images', 'backgrounds')
    files_back = listdir(dir_path)
    path_back = choice(files_back)
    back_img = OnscreenImage(
        image=loader.loadTexture(join(dir_path, path_back)),
        pos=(0, 0, 0),
    )
    back_img.reparentTo(aspect2d)
    screen_size = base.win.getXSize(), base.win.getYSize()
    image_size = back_img.getTexture().getXSize(), back_img.getTexture().getYSize()
    scale = image_size[1] / screen_size[1] if screen_size[1] < image_size[1] else screen_size[1] / image_size[1]
    scale1 = image_size[0] / screen_size[0] if screen_size[0] < image_size[0] else screen_size[0] / image_size[0]
    back_img.setScale(scale)
    print(str(screen_size))
    print(str(image_size))
    return back_img


def _menu_point(fun_load, fun_option, fun_exit):
    opt_font = loader.loadFont(join('fonts', 'RussoOne-Regular.ttf'))
    opt_font.setPixelsPerUnit(120)

    points = {
        tn(t("Start game")[0], opt_font): fun_load,
        tn(t("Options")[0], opt_font): fun_option,
        tn(t("Exit")[0], opt_font): fun_exit
    }

    def _command(arg):
        points[arg]()

    options = DirectOptionMenu(
        font=opt_font,
        items=list(points.keys()),
        highlightColor=(0.5, 0, 0, 1)
    )
    return options, opt_font


def MainMenu(fun_load, fun_option, fun_exit):
    # background_obj = _background()
    head_obj, head_font = _head_text()
    points, opt_font = _menu_point(fun_load, fun_option, fun_exit)
