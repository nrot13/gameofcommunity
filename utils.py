from pandac.PandaModules import loadPrcFileData
from panda3d.core import TextNode


def set_window(settings):
    loadPrcFileData("", "window-title GameOfCommunity")
    if "window" in settings and "sizes" in settings:
        loadPrcFileData("", "fullscreen {0}".format(int(not settings["window"])))  # Set to 1 for fullscreen
        loadPrcFileData("", "win-size {0} {1}".format(settings["sizes"]["x"], settings["sizes"]["y"]))
        loadPrcFileData("", "win-origin 100 100")
    loadPrcFileData("", "textures-power-2 pad")


# Высота становиться конечной, а ширина скалируется с тем же коэфициентом, возвращаются коэфициэнты
#                (x, y),   (x, y)
def resize_image_scale(fromSize, toSize):
    k = fromSize[1] / toSize[1]
    x = fromSize[0] / k
    y = fromSize[1] / k
    k0 = x / fromSize[0]
    k1 = y / fromSize[1]
    return k0, k1


def text_to_node(text, font):
    tn = TextNode(text)
    tn.setFont(font)
    return tn
